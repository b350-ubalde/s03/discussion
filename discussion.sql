-- MySQL CRUD Operations


-- [SECTION] Inserting a Record
    -- Syntax:
        -- INSERT INTO table_name (column_name) VALUES (value1);

    INSERT INTO artists (artist_name) VALUES ("Psy");
    INSERT INTO artists (artist_name) VALUES ("Rivermaya");

    -- Inserting record with multiple columns
    -- Syntax:
        -- Syntax should be in order as the structure of the table
        -- INSERT INTO table_name (column_nameA, column_nameB, column_nameC...) VALUES (valueA, valueB...);

    -- Albums Table
    INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2013-08-15", 1);
    INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-02-14", 2);

    -- Songs Table
    INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 339, "K-pop", 1);
    INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 524, "OPM", 2);
    INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 441, "OPM", 2);

        --MySQL non-delimeter Date and Time literals
            -- This allows Date and Timevalues to represented without delimeters (such as hyphens, colons, or spaces)
            -- Time Format
                -- Original: HH:MM:SS
                -- Non-delimiter: HHMMSS
                -- But make sure that it still follow the correct "max" value for each time intervals:
                    -- HH = 24
                    -- MM = 59
                    -- SS = 59

    -- Adding multiple records in 1 query
    --SYNTAX:
        -- INSERT INTO table_name (columnA, columnB, columnC, ...) VALUES (value1, value2), (value1, value2)

    INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 524, "OPM", 2), ("Kisapmata", 441, "OPM", 2);


-- [SECTION] Selecting records
    -- Showing all record details
        -- SELECT * FROM table;
            -- * means all columns will be shown in the selected table.

        SELECT * FROM songs;
    
    -- Show records with selected columns
    -- SELECT columnA, columnB, .... FROM table_name;

    -- Display the title and genre of all songs
    SELECT song_name, genre FROM songs;

    -- Display album title and date released of all albums
    SELECT album_title, date_released FROM albums;

    -- Show record that meets a certain condition
        -- SYNTAX:
        -- SELECT columnA FROM table_name WHERE condition;
        -- WHERE clause is used to filter records and to extract only those records that fulfill a specific condition.
        -- WHERE clauseis not case-sensitive

    -- Display all songs under OPM genre:
    SELECT song_name FROM songs WHERE genre = "OPM";

    -- MINI ACTIVITY
    -- Display title of all songs where length is less than 4 mins
    SELECT song_name FROM songs WHERE length < 400;

    -- Show records with multiple conditions:
    -- SYNTAX:
        -- AND clause
        --SELECT column_name FROM table_name WHERE condition1 AND condition2;

    -- Display the title and length of the OPM songs that are more than 4 mins and 30 seconds
    SELECT song_name FROM songs WHERE length > 430 AND genre = "OPM";


    -- [SECTION] Updating Records
        -- Updating single column of a record
        -- SYNTAX:
        -- UPDATE table_name SET column_name = new_value WHERE condition;

        -- Update the length of Kundiman to 4 minutes 24 secs
        UPDATE songs SET length = 424 WHERE song_name = "Kundiman";

        -- Check if Kundiman is updated
        SELECT song_name, length FROM songs WHERE song_name = "Kundiman";

        -- Update multiple columns of records
        -- SYNTAX:
        -- UPDATE table_name SET column_name1 = new_value1, column_name2 = new_value2 WHERE condition;

        -- Update the album with a title Psy 6 to "Psy6 (Six Rules)" and date released to July 15, 2012
        UPDATE albums SET album_title = "Psy 6 (Six Rules)", date_released = 20120715 WHERE album_title = "Psy 6";


    -- [SECTION] Delete a record
    -- SYNTAX:
    -- DELETE FROM table_name WHERE condition;
        -- NOTE: Removing the WHERE clause will remove all rows in the table
    
    -- Delete all OPM songs that are more than 4 minutes and 30 seconds
    DELETE FROM songs WHERE genre = "OPM" AND length > 430;

    -- check if song is deleted
    SELECT * FROM songs;